extends Item

class_name InventoryItem

func _init(level: int).(level) -> void:
	# set default icon
	self.icon = load("res://assets/hud/inventory/item_icons/nothing.png")
	
	# set name
	self.name = "none"
	
	# set level
	self.level = level
	
func set_icon_path(path):
	self.icon = load(path)
	
func set_name(name):
	self.name = name
