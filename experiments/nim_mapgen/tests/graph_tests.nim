
# std lib
import unittest
import sequtils
import sugar

# app
import graph

suite "Test Graph Map":
  test "Stupidest map":
    func to_b(n: Node[char]): char =
      return 'b'

    let
      (g, _) = single_node_graph('a')
      mapped_g = map(g, to_b)
    assert all_nodes(mapped_g).all(n => n.value == 'b')
