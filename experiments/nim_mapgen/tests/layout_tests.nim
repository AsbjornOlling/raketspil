
# std lib
import unittest

# app imports
import gamemaths
import graph
import abstractroom
import layout


suite "Test force-driven graph layout":
  test "Layout 2-node chain":
    let
      abstract = minimal_level()
      laid_out = place_abstract_rooms(abstract)
    assert not graph_overlaps(laid_out)

  test "Layout 4-node cycle":
    let
      abstract = four_cycle_level()
      laid_out = place_abstract_rooms(abstract)
    assert not graph_overlaps(laid_out)

  # test "Layout double 4-node cycle":
  #   let
  #     abstract = double_four_cycle_level()
  #     laid_out = place_abstract_rooms(abstract)
  #   assert not graph_overlaps(laid_out)

  # test "Layout quadruple 4-node cycle":
  #   let
  #     abstract = quad_four_cycle_level()
  #     laid_out = place_abstract_rooms(abstract)
  #   assert not graph_overlaps(laid_out)


suite "Test `graph_overlaps()`":
  setup:
    let
      stupidrect = Rect[float](topleft: Point[float](x: 0, y: 0), width: 10, height: 10)
      a = LayoutRoom(bounds: stupidrect)
      (a_graph, a_node) = single_node_graph(a)

      b_rect = stupidrect.translate(Vector[float](x: 7, y: 7))
      b = LayoutRoom(bounds: b_rect)

      c_rect = stupidrect.translate(Vector[float](x: 100, y: 100))
      c = LayoutRoom(bounds: c_rect)

  test "Two identical rects overlap":
    let
      (graph, _) = a_graph.connect_node(a_node, a)
    assert graph_overlaps(graph)

  test "Two different rects overlap":
    let (graph, _) = a_graph.connect_node(a_node, b)
    assert a.bounds != b.bounds
    assert all_nodes(graph).len() == 2
    assert graph_overlaps(graph)

  test "Far rects dont overlap":
    assert not overlaps(a.bounds, c.bounds)
    let (graph, _) = a_graph.connect_node(a_node, c)
    assert not graph_overlaps(a_graph)

  test "More complicated overlap":
    assert overlaps(a.bounds, b.bounds)
    assert not overlaps(a.bounds, c.bounds)
    assert not overlaps(b.bounds, c.bounds)
    let
      (g1, c_node) = a_graph.connect_node(a_node, c)
      (g2, _) = g1.connect_node(c_node, b)
    assert graph_overlaps(g2)
