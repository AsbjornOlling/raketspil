
#[ ASCII RENDER

Render a room graph to ASCII.
Really only used for development.
Actual game will use a different render.

]#

# std lib

# app imports
import tiles

func render_ascii*(tmap: TileMap): string =
  var str: string
  for y in 0..(len(tmap)-1):
    for x in 0..(len(tmap[y])-1):
      case tmap[y][x]:
        of FloorTile:
          str &= "."
        of WallTile:
          str &= "#"
        of EmptyTile:
          str &= "░"
    str &= "\n"
  return str
