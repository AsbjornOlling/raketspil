import tables, sugar, sequtils

type
  NodeID = int

  Edge = tuple[a: NodeID, b: NodeID]

  Graph*[T] = object
    nodes: Table[NodeID, T]
    edges: seq[Edge]

  Node*[T] = tuple[id: NodeID, value: T]

  Connection*[T] = tuple[a: Node[T], b: Node[T]]


#[ INTERNAL ]#

func get_node[T](g: Graph[T], id: NodeID): Node[T] =
  ## Pulls node out of graph table into `Node` tuple
  (id: id, value: g.nodes[id])


func `&`*[T](a, b: Table[NodeID, T]): Table[NodeID, T] =
  result = a
  for (k, v) in b.pairs():
    result[k] = v


#[ READ GRAPH ]#

func all_nodes*[T](g: Graph[T]): seq[Node[T]] =
  ## All `Node`s in the `Graph`
  let
    nodes: seq[Node[T]] = collect(new_seq):
      for i, v in g.nodes.pairs(): (id: i, value: v)
  return nodes


func neighbors*[T](g: Graph[T], node: Node[T]): seq[Node[T]] =
  ## Iterate over neighbors for a specific node.
  collect(new_seq):
    for edge in g.edges:
      # if self found, return other
      if node.id == edge.a:
          g.get_node(edge.b)
      elif node.id == edge.a:
          g.get_node(edge.b)


func contains*[T](g: Graph[T], n: Node[T]): bool =
  ## Whether a Node is present in a given Graph
  n.id in g.nodes


func connections*[T](g: Graph[T]): seq[Connection[T]] =
  ## Get pairs of connected nodes
  g.edges.map(e => (a: g.get_node(e.a), b: g.get_node(e.b)))


#[ THE BEST FUNCTION ]#

func map*[A,B](g: Graph[A], f: (Node[A]) -> B): Graph[B] =
  ## Map a function over all nodes in the graph
  var newnodes: Table[NodeID, B] = init_table[NodeID, B]()
  for n in all_nodes(g):  # maybe this should be done with `collect()`
    newnodes[n.id] = f(n) # but I couldn't get it to work..
  # return the mapped graph
  Graph[B](
    nodes: newnodes,
    edges: g.edges
  )


#[ MAKE AND MODIFY GRAPHS ]#

func single_node_graph*[T](value: T): (Graph[T], Node[T]) =
  ## Make a Graph, return the graph and the single node
  ## Useful for starting a graph from scratch
  ## Returns the graph and a Node object with the attached node
  let
    id: NodeID = 0
    graph = Graph[T](
      nodes: {id : value}.to_table(),
      edges: new_seq[Edge]()
    )
    node: Node[T] = (id: id, value: value)
  (graph, node)


func connect_node*[T](g: Graph[T],
                      connect_node: Node[T],
                      value: T): (Graph[T], Node[T]) =
  ## Add a value to the graph, connecting it to an existing Node
  ## Returns the resulting graph, and the newly constructed Node.
  let
    new_id: NodeID = len(g.nodes)
    new_edge: Edge = (a: connect_node.id, b: new_id)
    new_node = (id: new_id, value: value)

  # add node to node table
  # (TODO: can this be done immutably?)
  var new_nodes = g.nodes
  new_nodes[new_id] = value

  # assemble new graph
  let new_graph = Graph[T](
    nodes: new_nodes,
    edges: @[new_edge] & g.edges
  )
  return (new_graph, new_node)


func add_edge*[T](g: Graph[T], a, b: Node[T]): Graph[T] =
  ## Adds an edge between two existing Nodes
  assert g.nodes.has_key(a.id)
  assert g.nodes.has_key(b.id)
  Graph[T](
      nodes: g.nodes,
      edges: @[(a: a.id, b: b.id)] & g.edges
  )


func connect_graphs*[T](g1, g2: Graph[T], n1, n2: Node[T]): Graph[T] =
  ## Connect two graphs, adding an edge between n1 and n2
  assert n1 in g1
  assert n2 in g2

  # first shift all NodeIDs in `g2` up, to avoid overlap
  let
    id_offset = len(g1.nodes)
    # new edges list from g2
    shifted_edges: seq[Edge] = map(g2.edges, edge => (a: edge.a + id_offset, b: edge.b + id_offset))

  # new nodetable from g2
  var shifted_nodes: Table[NodeID, T] = init_table[NodeID, T]()
  for (k, v) in g2.nodes.pairs():
    shifted_nodes[k + id_offset] = v

  # then assemble into one graph
  Graph[T](
      nodes: g1.nodes & shifted_nodes,
      edges: g1.edges & shifted_edges & @[(a: n1.id, b: n2.id + id_offset)]
  )
