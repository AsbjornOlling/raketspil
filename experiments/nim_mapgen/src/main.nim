
#[ Map Gen overview:
1. Generate a tree of abstract rooms - graph grammar may help here
  TODO: implement graph grammar system
    try to implement:
      A => A->B
      A->B => A->c->B
      A->B => A->C->B
                 |
                 v
                 D
    
  graph grammar system supports only:
    - matching subgraphs with one entry node and one exit node

2. Assign concrete tiles to rooms + hallways, and lay them out on a grid
    x this is where we give the rooms shapes
    x make sure they dont overlap
    x make sure they're laid out in a sensible order
]#

import random

# local imports
import graph
import abstractroom
import layout
import tiles
import asciirender

#[ ASCII RENDERING STUFF ]#

proc new_ascii_map(): cstring {.exportc.} =
  ## This generates a new map, and renders it out to ASCII.
  ## (The {.exportc.} pragma lets this function be called from JS).
  randomize()
  let
    # minimal level
    # abstract = minimal_level()
    #
    # 4-cycle
    # abstract: Graph[AbstractRoom] = four_cycle_level()
    #
    # double 4-cycle
    # abstract: Graph[AbstractRoom] = double_four_cycle_level()
    #
    # quad 4-cycle
    abstract: Graph[AbstractRoom] = quad_four_cycle_level()

    # lay out in non-overlapping way
    placed_rooms: Graph[LayoutRoom] = place_abstract_rooms(abstract)

    # assign tiles to rooms
    tiles: TileMap = tile_graph(placed_rooms)

    # render to text
    printable: string = render_ascii(tiles)

  return printable
