#[ LEVEL ]#
# A `Level` is a graph of abstract rooms.
# `Level` represents a level, that hasn't been given an exact layout yet.

# app imports
import graph

type
  RoomType* = enum
    StartRoom,
    EndRoom,
    EmptyRoom

  AbstractRoom* = object
    roomtype*: RoomType

func minimal_level*(): Graph[AbstractRoom] =
  ## A level that consists of only a start room and an end room.
  let
    startroom = AbstractRoom(
        roomtype: StartRoom
    )
    endroom = AbstractRoom(
        roomtype: EndRoom
    )
    (startgraph, startnode) = single_node_graph[AbstractRoom](startroom)
    (finalgraph, _) = startgraph.connect_node(startnode, endroom)
  finalgraph


func four_cycle_level*(): Graph[AbstractRoom] =
  ## A level with 4 rooms in a cycle
  let
    # make four rooms
    a = AbstractRoom(roomtype: StartRoom)
    b = AbstractRoom(roomtype: EmptyRoom)
    c = AbstractRoom(roomtype: EmptyRoom)
    d = AbstractRoom(roomtype: EndRoom)

    # connect them in a graph
    # TODO: can this be generalized into a fold?
    (g0, a_node) = single_node_graph(a)
    (g1, b_node) = g0.connect_node(a_node, b)
    (g2, c_node) = g1.connect_node(b_node, c)
    (g3, d_node) = g2.connect_node(c_node, d)
    finalgraph = g3.add_edge(d_node, a_node)
  finalgraph


func double_four_cycle_level*(): Graph[AbstractRoom] =
  ## Makes a level of two cycles, 
  let
    cycle = four_cycle_level()
    doublecycle = connect_graphs(cycle, cycle,
                                 all_nodes(cycle)[0],
                                 all_nodes(cycle)[0])
  assert len(all_nodes(doublecycle)) == 8
  return doublecycle


func quad_four_cycle_level*(): Graph[AbstractRoom] =
  ## Makes a level of four cycles, 
  let
    double = double_four_cycle_level()
    quad = connect_graphs(double, double,
                          all_nodes(double)[0],
                          all_nodes(double)[0])
  assert len(all_nodes(quad)) == 16
  return quad

