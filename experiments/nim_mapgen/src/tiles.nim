#[ TILES ]#
# Make tiles for rooms
# First assign tiles to rooms and blit them onto an empty canvas
# XXX: WIP then draw hallways over the map, cutting through rooms

# std lib imports
import sugar
import sequtils
import math

# own imports
import graph
import layout
import gamemaths


type
  Tile* = enum
    WallTile,
    FloorTile,
    EmptyTile

  TileMap* = seq[seq[Tile]]

  Blittable = tuple[tiles: TileMap, pos: Point[int]]


func new_tilemap(w, h: int, tile: Tile): TileMap =
  # this seems like a fucking bass-ackwards way of doing it
  # what is the clean way?
  var tmap: TileMap = newSeq[seq[Tile]](h)
  for y in 0..(h-1):
    var row = newSeq[Tile](w)
    for x in 0..(w-1):
      row[x] = tile
    tmap[y] = row
  assert w > 0 and h > 0
  assert len(tmap) == h and all(tmap, t => len(t) == w)
  return tmap


func blit(tmap: TileMap, b: Blittable): TileMap =
  ## Place a blittable onto a TileMap
  # check that blittable is smaller
  assert len(b.tiles) <= len(tmap)
  assert len(b.tiles[0]) <= len(tmap[0])
  # check that blittable fits inside tmap
  assert len(b.tiles) + b.pos.y <= len(tmap)
  assert len(b.tiles[0]) + b.pos.x <= len(tmap[0])

  # TODO: this should be a comprehension, no?
  var new_tmap: TileMap = tmap
  for y in 0..(len(b.tiles)-1):
    for x in 0..(len(b.tiles[y])-1):
      new_tmap[b.pos.y+y][b.pos.x+x] = b.tiles[y][x]
  return new_tmap


func soft_blit(tmap: TileMap, b: Blittable): TileMap =
  ## Blit `b` on `tmap`, but letting FloorTile hold priority
  # check that blittable is smaller
  assert len(b.tiles) <= len(tmap)
  assert len(b.tiles[0]) <= len(tmap[0])
  # check that blittable fits inside tmap
  assert len(b.tiles) + b.pos.y <= len(tmap)
  assert len(b.tiles[0]) + b.pos.x <= len(tmap[0])

  var new_tmap: TileMap = tmap
  for y in 0..(len(b.tiles)-1):
    for x in 0..(len(b.tiles[y])-1):
      let
        oldtile = tmap[b.pos.y+y][b.pos.x+x]
        newtile = b.tiles[y][x]
      if oldtile == FloorTile:
        continue
      if newtile == EmptyTile:
        continue
      new_tmap[b.pos.y+y][b.pos.x+x] = newtile
  return new_tmap


func move_above_zero(g: Graph[Rect[int]]): Graph[Rect[int]] =
  ## Translate all rects to at coords >= 0
  ## Useful b/c then we can do array indexing
  let
    rects = g.all_nodes().map(n => n.value)
    # get bounds of map
    min_x: int = rects.map(r => r.topleft.x).min()
    min_y: int = rects.map(r => r.topleft.y).min()
    # make translation vector
    vec = Vector[int](x: -1 * min_x, y: -1 * min_y)
  return g.map(n => n.value.translate(vec))


func floor_tile(rect: Rect[int]): Blittable =
  (
      tiles: new_tilemap(rect.width, rect.height, FloorTile),
      pos: rect.topleft
  )


func tile_rect_room(rect: Rect[int]): Blittable =
  let
    # make floor
    floor = floor_tile(rect)

    # make walls
    topwall = (
      tiles: new_tilemap(rect.width, 1, WallTile),
      pos: Point[int](x: 0, y: 0)
    )
    bottomwall = (
      tiles: new_tilemap(rect.width, 1, WallTile),
      pos: Point[int](x: 0, y: rect.height-1)
    )
    leftwall = (
      tiles: new_tilemap(1, rect.height, WallTile),
      pos: Point[int](x: 0, y: 0)
    )
    rightwall = (
      tiles: new_tilemap(1, rect.height, WallTile),
      pos: Point[int](x: rect.width-1, y: 0)
    )
    walls = @[topwall, bottomwall, leftwall, rightwall]

    # blit walls on floor to make final room tiles
    room_tiles = foldl(walls, blit(a, b), floor.tiles)
  (
      tiles: room_tiles,
      pos: rect.topleft
  )


#[ CORRIDORS ]#
# XXX: SUPER WIP
# - all roomnodes should have valid door placements defined
# - for each edge in tree:
#     find closest door pair
#     XXX: draw 3 wide corridor
#     force blit over tmap
#     return new tmap

func dx_dy(a, b: Point[int]): (int, int) =
  (abs(b.x - a.x), abs(b.y - a.y))


func single_width_line(a, b: Point[int], tile: Tile): Blittable =
  ## Draw a single width Bresenham line going from a to b
  let
    (dx, dy) = dx_dy(a, b)
    sx = if a.x < b.x: 1 else: -1
    sy = if a.y < b.y: 1 else: -1
    xoff = min([a.x, b.x])
    yoff = min([a.y, b.y])

  var
    p = a
    b = b
    err = (if dx > dy: dx else: -dy) div 2
    e2 = 0
    tiles = new_tilemap(dx+1, dy+1, EmptyTile)

  while true:
    tiles[p.y-yoff][p.x-xoff] = tile
    if p == b:
      break
    e2 = err
    if e2 > -dx:
      # step sideways
      err -= dy
      p.x += sx
    if e2 < dy:
      # step up
      err += dx
      p.y += sy

  return (
      tiles: tiles,
      pos: Point[int](x: xoff, y: yoff)
  )


func make_corridors(g: Graph[Rect[int]]): seq[Blittable] =
  ## Make blittable corridor tiles for a graph
  func thicc_corridor(con: Connection[Rect[int]]): seq[Blittable] =
    let
      WIDTH = 5
      a = con.a.value.center()
      b = con.b.value.center()
      (dx, dy) = dx_dy(a, b)
      stackdir =
        if (dx / dy) > 1:
          Vector[int](x: 0, y: 1)
        else:
          Vector[int](x: 1, y: 0)
      floor_offs = to_seq(1..(WIDTH-2)).map(i => float(i) * stackdir)
      wall_offs = @[0, WIDTH-1].map(i => float(i) * stackdir)
      flines = floor_offs.map(v => single_width_line(a.translate(v),
                                                     b.translate(v),
                                                     FloorTile))
      wlines = wall_offs.map(v => single_width_line(a.translate(v),
                                                    b.translate(v),
                                                    WallTile))
    return flines & wlines
  return g.connections().map(thicc_corridor).concat()


#[ this is the main function to be used in mapgen pipeline ]#
func tile_graph*(g: Graph[LayoutRoom]): TileMap =
  ## Take a floating abstract room graph and give it concrete tiles and hallways
  let
    grid_rects_graph: Graph[Rect[int]] =
      g.map(n => n.value.bounds.round_to_int()).move_above_zero()

    rects = all_nodes(grid_rects_graph).map(n => n.value)

    # calculate map size
    max_x: int = rects.map(r => r.topleft.x + r.width).max()
    max_y: int = rects.map(r => r.topleft.y + r.height).max()
    w: int = max_x + 2  # XXX: +2 should really be +1...
    h: int = max_y + 2  # XXX: but theres an off-by-one I can't find so

    # tile each room individually
    tiled_rooms: seq[Blittable] = rects.map(tile_rect_room)

    # make corridors 
    corridors: seq[Blittable] = make_corridors(grid_rects_graph)

    # make empty map tilemap
    emptymap: TileMap = new_tilemap(w, h, EmptyTile)

  # fold blit on tiled rooms
  foldl(tiled_rooms & corridors, soft_blit(a, b), emptymap)
