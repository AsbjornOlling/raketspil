
#[ LAYOUT GRAPH ]# 

# - take a fully-constructed abstract room graph
# - make rects surrounding each room
# - use force-directed approach to pull rooms towards neighbors
#   basically a physics simulation that
#     - simulates spring forces in place of all edges
#     - simulates repulsion forces between all nodes
#   (this is a heuristic approach and might fail for bigger graphs)
#
#[ ROOM FLOATING ]#
# - for each room:
#   - calculate an attractive vector to each neighbor
#   - calculate a repulsion vector to neighbors within a certain range
# - move all rooms
# - return


# std lib
import math
import sequtils
import sugar
import random

# app imports
import graph
import gamemaths
import abstractroom

type
  LayoutRoom* = object
    room: AbstractRoom
    bounds*: Rect[float]


func graph_overlaps*(g: Graph[LayoutRoom]): bool =
  ## Checks an entire graph for overlaps
  ## (Only useful for quality assurance after room layout is complete)
  # get all bounds
  let rects: seq[Rect[float]] = g.all_nodes().map(x => x.value.bounds)

  func overlaps_any_others(r: Rect[float]): bool =
    # find self
    let selfidx: int = rects.find(r)
    # make list of others
    var others: seq[Rect[float]] = rects
    others.delete(selfidx)
    return others.any(x => overlaps(r, x))

  return rects.map(overlaps_any_others).any(x => x)


#[ ROOM FLOATING STUFF ]#

const
  # for attraction
  ATTRACTION_FORCE: float = 20.0
  PAD_IDEAL_DIST: float = 3.0
  # for repulsion
  MIN_REPULSION_DIST: float = 1.0
  REPULSION_FORCE: float = 1.0
  # used in the lopo
  ITERATIONS: int = 1000
  STEP_SPEED: float = 0.5


func attraction_vectors(g: Graph[LayoutRoom],
                        node: Node[LayoutRoom]): seq[Vector[float]] =
  ## Spring vectors from the center of r to its neighbors' centers
  let this_rect: Rect[float] = node.value.bounds
  func vec_to(n: Node[LayoutRoom]): Vector[float] =
    # spring vec
    let
      other_rect = n.value.bounds
      idealdist = (PAD_IDEAL_DIST +
                   (biggest_dimension(this_rect).float() / 2) +
                   (biggest_dimension(other_rect).float() / 2))
      dist = distance(this_rect.center(), other_rect.center())
      force = ATTRACTION_FORCE * log10(dist / idealdist)
      angle = this_rect.center().to(other_rect.center()).angle()
    return new_vector(angle, force)
  return g.neighbors(node).map(vec_to)


func repulsion_vectors(g: Graph[LayoutRoom],
                       node: Node[LayoutRoom]): seq[Vector[float]] =
  ## Get repulsion vectors for all rooms in range 
  let
    this_rect: Rect[float] = node.value.bounds
    other_rects: seq[Rect[float]] = all_nodes(g)
        .filterIt(it != node)
        .map(n => n.value.bounds)
  func get_vec(other: Rect[float]): Vector[float] =
    let
      dist = max([
          MIN_REPULSION_DIST,
          distance(this_rect.center(), other.center()),
      ])
      force = REPULSION_FORCE / (dist^2)
      angle = other.center().to(this_rect.center).angle()
      vec = new_vector(angle, force)
    return vec
  return other_rects.map(get_vec)


func float_rooms(g: Graph[LayoutRoom]): Graph[LayoutRoom] =
  ## Float nodes around to find optimal layout.
  ## Just do it `ITERATION` times - its fiiine
  func float_room(n: Node[LayoutRoom]): LayoutRoom =
    ## Float a single node in the graph
    let
      vec = STEP_SPEED * sum(attraction_vectors(g, n) & repulsion_vectors(g, n))
      newbounds = n.value.bounds.translate vec
    return LayoutRoom(
      bounds: newbounds,
      room: n.value.room
    )
  # do the simulation `ITERATION` times
  # TODO: can we fold here?
  result = g
  for i in 1..ITERATIONS:
    result = result.map(float_room)


#[ ROOM CONVERSION
#  AND EXPOSED FUNCTIONS ]#

proc random_rect(): Rect[float] =
  ## Returns a random rect within some config bounds
  let
    MIN_SIZE = 5
    MAX_SIZE = 15
    POS_VARIANCE = 5
  Rect[float](
      width:   rand(MIN_SIZE..MAX_SIZE),
      height:  rand(MIN_SIZE..MAX_SIZE),
      topleft: Point[float](
          x: rand(-POS_VARIANCE..POS_VARIANCE).float(),
          y: rand(-POS_VARIANCE..POS_VARIANCE).float()
      )
  )


proc to_layout_graph(g: Graph[AbstractRoom]): Graph[LayoutRoom] =
  ## Make bounds for a room.
  ## For now all rooms are just random rects of the same size
  # TODO: different shapes and sizes
  proc to_layoutroom(n: Node[AbstractRoom]): LayoutRoom =
    LayoutRoom(
        bounds: random_rect(),
        room: n.value
    )
  g.map(to_layoutroom)


proc place_abstract_rooms*(g: Graph[AbstractRoom]): Graph[LayoutRoom] =
  ## Convert to LayoutRoom graph, then float the rooms into place
  ## This is the function that should be used from other modules
  g.to_layout_graph().float_rooms()
