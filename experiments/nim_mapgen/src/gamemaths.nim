import math

#[ GAMEMATHS ]#
# this file has some common maths stuffs
# mostly just geometry things
# all types can be in either int or float representations

type
  Number* = int | float

  Point*[T: Number] = object
    x*, y*: T

  Rect*[T: Number] = object
    topleft*: Point[T]
    width*, height*: int

  Vector*[T: Number] = object
    x*, y*: T


#[ POINT ]#

func distance*(a, b: Point[Number]): float =
  ## Euclidean distance between points
  (float(a.x - b.x)^2 + float(a.y - b.y)^2).sqrt()

func translate*[T: Number](p: Point[T], v: Vector): Point[T] =
  ## Point translated by vector
  Point[T](
      x: p.x + v.x,
      y: p.y + v.y
  )

func round_to_int*(fp: Point[float]): Point[int] =
  Point[int](
      x: fp.x.round().int(),
      y: fp.y.round().int()
  )


#[ VECTOR ]#

func `+`*(a, b: Vector): Vector =
  Vector(
      x: a.x + b.x,
      y: a.y + b.y
  )

func `*`*(f: float, b: Vector[int]): Vector[int] =
  Vector[int](
      x: int(f * float(b.x)),
      y: int(f * float(b.y))
  )

func `*`*(f: float, b: Vector[float]): Vector[float] =
  Vector[float](
      x: f * b.x,
      y: f * b.y
  )

func len*(v: Vector[float]): float =
  ## Float length of vector
  ((v.x^2) + (v.y^2)).float().sqrt()

func angle*[T: Number](v: Vector[T]): float =
  ## Vector angle in radians
  arctan2(float(v.y), float(v.x))

func new_vector*(theta: float, l: float): Vector[float] =
  ## Returns vector of length `l` at `theta` radians
  Vector[float](
      x: l * cos(theta),
      y: l * sin(theta)
  )

func to*[T: Number](a, b: Point[T]): Vector[T] =
  # Returns the Vector going from `a` to `b`
  Vector[T](
      x: b.x - a.x,
      y: b.y - a.y
  )


#[ RECT ]#

func center*(rect: Rect[float]): Point[float] =
  ## Center point of `rect`
  Point[float](
    x: rect.topleft.x + rect.width / 2,
    y: rect.topleft.y + rect.height / 2
  )

func center*(rect: Rect[int]): Point[int] =
  Point[int](
    x: rect.topleft.x + int(float(rect.width) / 2),
    y: rect.topleft.y + int(float(rect.height) / 2)
  )

func overlaps*[T: Number](a, b: Rect[T]): bool =
  ## True if `a` and `b` are clear of each other (i.e. no overlapping)
  let
    a_left = (a.topleft.x + T(a.width)) < b.topleft.x
    a_right = (b.topleft.x + T(b.width)) < a.topleft.x
    a_above = (a.topleft.y + T(a.height)) < b.topleft.y
    a_below = (b.topleft.y + T(b.height)) < a.topleft.y
  return not (a_left or a_right or a_above or a_below)

func translate*[T: Number](rect: Rect[T], vec: Vector): Rect[T] =
  Rect[T](
    topleft: rect.topleft.translate(vec),
    width: rect.width,
    height: rect.height
  )

func biggest_dimension*[T: Number](rect: Rect[T]): int =
  max [rect.width, rect.height]

func round_to_int*(rect: Rect[float]): Rect[int] =
  Rect[int](
    topleft: round_to_int(rect.topleft),
    width: rect.width,
    height: rect.height
  )
