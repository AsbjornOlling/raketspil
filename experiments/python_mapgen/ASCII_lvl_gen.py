'''

"_" = blank part of the canvas to be overwritten with level parts; this is not a part of any level
"." = blank part of a level; the part of the level you can fly around in
"w" = wall
"d" = door

'''


canvas_20x20 = [["_" for i in range(20)] for i in range(20)]

level_door_top = \
          [["w", "w", "d", "w", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", "w", "w", "w", "w"]]

level_door_right = \
          [["w", "w", "w", "w", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "d"],
           ["w", ".", ".", ".", "w"],
           ["w", "w", "w", "w", "w"]]

level_door_bottom = \
          [["w", "w", "w", "w", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", "w", "d", "w", "w"]]

level_door_left = \
          [["w", "w", "w", "w", "w"],
           ["w", ".", ".", ".", "w"],
           ["d", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", ".", ".", ".", "w"],
           ["w", "w", "w", "w", "w"]]

def print_all(array):
    for row in array:
        print(row)

def canvas_has_space_for_a_level(canvas, level, row_no, level_starting_pos):
    if space_on_x_axis(canvas, level, row_no, level_starting_pos) and \
        space_on_y_axis(canvas, level, level_starting_pos):
        return True
    else:
        return False

def space_on_x_axis(canvas, level, row_no, level_starting_pos):
    x_1 = len(canvas[row_no])
    x_2 = level_starting_pos[0] + len(level[row_no])
    x_3 = x_1 - x_2
    if x_3 >= 0:
        return True
    else:
        return False

def space_on_y_axis(canvas, level, level_starting_pos):
    y_1 = len(canvas)
    y_2 = level_starting_pos[1] + len(level)
    y_3 = y_1 - y_2
    if y_3 >= 0:
        return True
    else:
        return False


def put_level_on_canvas(level, canvas, level_starting_pos):
    for row_no, row in enumerate(canvas[:len(level)]):
        if canvas_has_space_for_a_level(canvas, level, row_no, level_starting_pos):
            for column_no, column in enumerate(row[:len(level[row_no])]):
                canvas[row_no + level_starting_pos[1]][column_no + level_starting_pos[0]] = level[row_no][column_no]





put_level_on_canvas(level_door_left, canvas_20x20, (15, 13))
print_all(canvas_20x20)
