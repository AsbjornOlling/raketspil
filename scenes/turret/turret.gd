extends StaticBody2D

# SHOOTING VARS
onready var arm_tip = get_node("Arm/Position2D")
onready var bulletscene = preload("res://scenes/turret/bullet.tscn")
var shooting = false
var shoot_timer = 0
export(int, 0, 100) var fire_rate = 50
export(int, 0, 100) var bullet_velocity = 60
export(int, 0, 60) var bullet_damage = 30

func bullet_vector(angle):
	var	x = cos(angle) * bullet_velocity
	var y = sin(angle) * bullet_velocity
	return Vector2(x, y)
	
func shoot(vector):
	# firing speed
	if shoot_timer == 0:
		# instance bullet
		var bullet = bulletscene.instance()
		bullet.set_starting_pos($Arm.position)
		add_child(bullet)
		bullet.velocity = vector
		shoot_timer += 1
	elif shoot_timer < fire_rate:
		shoot_timer += 1
	else:
		shoot_timer = 0
		
	

func _process(delta):
	var player = get_node("/root/Root/Player")
	var targetrotation = (player.position - position).angle()
	var degrees = rad2deg(targetrotation)
	
	
	
	# feel free to clean up this code lol.
	# do not change the name of rotation_degrees unless
	# it's for changing all of this to radians. It's snake case,
	# but it's a built in function of Godot.
	if rotation_degrees < 91: # if turret faces right or down:
		if degrees > (rotation_degrees - 89) and degrees < (rotation_degrees + 89):
			$Arm.rotation_degrees = degrees - rotation_degrees
			if shooting == true:
				shoot(bullet_vector($Arm.rotation))
				
				
			
			
			
#	elif rotation_degrees > 179 and rotation_degrees < 181: #turret faces down:
#		if degrees > 90 or degrees < -90:
#			$Arm.rotation_degrees = degrees - rotation_degrees
#			if shooting == true:
#				shoot(bullet_vector($Arm.rotation))
#
#
#
#	elif rotation_degrees > 269 and rotation_degrees < 271: #turret faces up:
#		if degrees > -180 and degrees < 0:
#			$Arm.rotation_degrees = degrees - rotation_degrees
#			if shooting == true:
#				shoot(bullet_vector($Arm.rotation))
#
	
	
	
func _on_DetectArea2D_body_entered(body):
	if body.name == "Player":
		shooting = true
		

func _on_DetectArea2D_body_exited(body):
	if body.name == "Player":
		shooting = false
