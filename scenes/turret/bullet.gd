extends Area2D

var velocity = Vector2(0, 0)

func set_starting_pos(pos):
	position = pos

func _process(delta):
	position += velocity * delta
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			body.lose_health(get_parent().bullet_damage)
			queue_free()
	if not $VisibilityNotifier2D.is_on_screen():
		queue_free()
