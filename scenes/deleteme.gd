extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	_make_shop(Vector2(0,0))
	_make_shop(Vector2(0,-64))

func _make_shop(pos: Vector2):
	# todo: take items as arguments
	var shopscene = preload("res://scenes/shop/shop.tscn")
	var shop = shopscene.instance()
	shop.initialize([FuelContainer.new(2), FuelContainer.new(0), FuelContainer.new(3)])
	shop.position = pos
	add_child(shop)
