extends AnimatedSprite
 

export(float, 0, 0.5) var SIZE_FACTOR = 0.1
export(float, 0, 10) var VISIBLE_THRESHOLD = 5  # in pixels / s

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var velocity = get_node("/root/Root/Player").velocity
	
	# visibility
	if velocity.length() < VISIBLE_THRESHOLD:
		visible = false
	else:
		visible = true

	# arrow size
	var framecount = frames.get_frame_count('arrow')
	var targetframe = int(velocity.length()) * SIZE_FACTOR
	if targetframe < framecount:
		frame = targetframe
	else:
		frame = framecount - 1

	# rotation
	set_rotation(velocity.angle() + (PI/2))
