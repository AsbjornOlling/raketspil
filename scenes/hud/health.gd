extends AnimatedSprite

var animtag = "13step"
var height = frames.get_frame(animtag, 0).get_size().y

func show_health_amount():
	var player = get_node("/root/Root/Player")
	var healthpct = player.health / player.max_health
	var framescount = frames.get_frame_count(animtag)
	frame = floor((1-healthpct) * framescount)

func _process(_delta):
	show_health_amount()
