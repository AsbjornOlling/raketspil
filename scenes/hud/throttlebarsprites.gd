extends AnimatedSprite

var animtag = 'neutral'
var height = frames.get_frame(animtag, 0).get_size().y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var player = get_node('/root/Root/Player')
	if player.throttle == 0:
		set_animation("neutral")
		frame = 0
	elif player.throttle > 0:
		set_animation("forward")
		frame = player.throttle - 1
	elif player.throttle < 0:
		set_animation("backward")
		frame = (player.throttle * -1) - 1
