extends CanvasLayer

onready var player = get_node("/root/Root/Player")
var height: int

func _ready():
	self.height = int($Fuel.height)
	
func _process(delta):
	$FuelText.text = "%.0f" %player.fuel
	$HealthText.text = "%.1f" %player.health
