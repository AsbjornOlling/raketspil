extends AnimatedSprite

var animtag = "13step"
var height = frames.get_frame(animtag, 0).get_size().y

func show_fuel_amount():
	var player = get_node("/root/Root/Player")
	var fuelpct = player.fuel / player.max_fuel
	var framescount = frames.get_frame_count(animtag)
	frame = floor((1-fuelpct) * framescount)
	

func _process(_delta):
	show_fuel_amount()
