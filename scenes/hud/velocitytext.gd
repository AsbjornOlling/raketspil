extends CanvasLayer

onready var height = int($Screen.get_rect().size.y)

func _process(_delta):
		var player = get_node('/root/Root/Player')
		var pixelspeed = player.velocity.length()
		var meterspeed = pixelspeed / 16
		$Text.text = "%.3f m/s" % meterspeed
