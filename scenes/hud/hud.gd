extends CanvasLayer

var ELEMENT_WIDTH = 64

func set_height_scale(ui_element: CanvasLayer, screenscale: float) -> void:
	# sets fuel bar height to be the viewport height times `scalecoeff`
	# this is called in `_process` (not `_ready`)
	# so that it'll scale on screen resize 
	var screenheight = get_viewport().size.y
	var target_element_height = screenheight * screenscale
	var target_element_scale = target_element_height / ui_element.height
	ui_element.set_scale(Vector2(target_element_scale, target_element_scale))


func align_left(ui_element: CanvasLayer) -> void:
	ui_element.offset.x = ui_element.scale.x * ELEMENT_WIDTH / 2


func align_right(ui_element: CanvasLayer) -> CanvasLayer:
	ui_element.offset.x = get_viewport().size.x - (ui_element.scale.x * ELEMENT_WIDTH / 2)
	return ui_element


func align_top(ui_element: CanvasLayer) -> CanvasLayer:
	ui_element.offset.y = ui_element.scale.y * ui_element.height / 2
	return ui_element


func align_bottom(ui_element: CanvasLayer) -> CanvasLayer:
	ui_element.offset.y = get_viewport().size.y - (element_height(ui_element) / 2)
	return ui_element


func element_height(ui_element: CanvasLayer) -> int:
	return ui_element.scale.y * ui_element.height


func stack_below(ui_elements):
	# stack elements vertically, with the first element in the list on top
	# first element keeps its y component, the rest get moved
	# pop top - do nothing if empty list
	var first: CanvasLayer = ui_elements.pop_front()
	if first == null:
		return
	
	# offset the rest of the elements
	var offsetpos: int = element_height(first)
	for e in ui_elements:
		e.offset.y = offsetpos + (element_height(e) / 2)
		offsetpos += element_height(e)


func stack_above(ui_elements):
	# stack elements verigcally, with the first element in the list on bottom
	var first: CanvasLayer = ui_elements.pop_front()
	if first == null:
		return
		
	# offset the res
	var offsetpos = first.offset.y - (element_height(first) / 2)
	for e in ui_elements:
		offsetpos -= (element_height(e) / 2)
		e.offset.y = offsetpos

func _process(_delta):
	# left panel	
	set_height_scale($Inventory, 0.5)
	align_left($Inventory)
	align_top($Inventory)
	
	# FuelBar still exists, in case we want to revert back to it
	
#	set_height_scale($FuelBar, 0.5)
#	align_left($FuelBar)
#	stack_below([$ThrottleBar, $FuelBar])
	
	set_height_scale($FuelAndHealth, 0.5)
	align_left($FuelAndHealth)
	align_bottom($FuelAndHealth)
	
	# right panel
	set_height_scale($Padding, 0.5)
	align_right($Padding)
	align_top($Padding)
	
	set_height_scale($VelocityText, 1/8.0)
	align_right($VelocityText)
	align_top($VelocityText)
	
	set_height_scale($VelocityScreen, 1/4.0)
	align_right($VelocityScreen)
	align_top($VelocityScreen)
		
	set_height_scale($ThrottleBar, 0.5)
	align_right($ThrottleBar)
	align_bottom($ThrottleBar)
	stack_above([$ThrottleBar, $VelocityScreen])
	stack_above([$VelocityScreen, $VelocityText])


