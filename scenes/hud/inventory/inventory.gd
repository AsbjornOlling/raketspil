extends CanvasLayer

onready var height: int = int($Screen.get_rect().size.y)
onready var player: KinematicBody2D = get_node("/root/Root/Player")

func _get_slots() -> Array:
	return $Slots.get_children()

func _show_slots(count: int) -> void:
	for slot in _get_slots():
		if count > 0:
			slot.visible = true
		else:
			slot.visible = false
		count -= 1

func _show_item(slot: Sprite, item: Item) -> void:
	# set item icon
	if item != null:
		var icon = slot.get_node('Icon')
		icon.texture = item.icon
		icon.visible = true
	
	# set item levels
	var level = slot.get_node('Level')
	if item != null:
		level.visible = true
		level.set_frame(item.level)
	else:
		level.visible = false
	
		


func _empty_slot(slot: Sprite) -> void:
	slot.get_node('Level').visible = false
	slot.get_node('Icon').visible = false


func _show_items(items: Array) -> void:
	var slots = _get_slots()
	for i in range(len(items)):
		_show_item(slots[i], items[i])
	for i in range(len(items), len(slots)):
		_empty_slot(slots[i])

func _process(_delta):
	_show_slots(player.inventory_size)
	# _show_items(player.inventory)
	_show_items(player.inventory)
