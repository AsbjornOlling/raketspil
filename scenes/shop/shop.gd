extends Area2D
class_name Shop


const BUBBLE_SELECTED_Y = -16
const N_ITEMS: int = 3
var selection: int = 0
var shop_items: Array = []

onready var player = get_node("/root/Root/Player")


func _ready() -> void:
	print("[DEBUG]: Shop _init")	
	# connect Area2D signals
	connect("body_entered", self, "enter_shop")
	connect("body_exited", self, "exit_shop")
	$AnimationPlayer.connect("animation_finished", self, "on_animation_finished")
	set_process(false)

func move_up(node: Node):
	var tween = node.get_node("Tween")
	tween.interpolate_property(node, "position",
								node.position,
								Vector2(node.position.x, BUBBLE_SELECTED_Y),
								1,
								Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
	tween.start()

func move_down(node: Node):
	var tween = node.get_node("Tween")
	tween.interpolate_property(node, "position",
								node.position,
								Vector2(node.position.x, 0),
								1,
								Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
	tween.start()

func initialize(items: Array):
	## This is the actual constructor
	## it MUST 
	print("[DEBUG]: Shop initialize")
	self.shop_items = items
	place_items(items)

func enter_shop(body):
	## Called when player enters shop
	print("[DEBUG] enter shop")
	if body.name != "Player":
		return
	body.in_shop = true
	set_process(true)
	move_up($Bubbles.get_child(selection))
	
func exit_shop(body):
	## Called when player leaves shop
	if body.name != "Player":
		return
	body.in_shop = false
	set_process(false)

func place_items(items: Array):
	## Put item icons inside the shop's bubbles
	for i in range(len(items)):
		print("[DEBUG] Item " + str(i))
		var bubble = $Bubbles.get_child(i)
		var item_sprite = bubble.get_node("ItemIcon")
		item_sprite.texture = items[i].icon
		item_sprite.set_scale(Vector2(0.5, 0.5))
		
func _process(_delta):
	input()

func input():
	if Input.is_action_just_pressed("ui_right"):
		var old_selection = $Bubbles.get_child(selection)
		selection = (selection + 1) % N_ITEMS
		var new_selection = $Bubbles.get_child(selection)
		move_down(old_selection)
		move_up(new_selection)
		
	if Input.is_action_just_pressed("ui_left"):
		var old_selection = $Bubbles.get_child(selection)
		selection = (selection - 1 + N_ITEMS) % N_ITEMS
		var new_selection = $Bubbles.get_child(selection)
		move_down(old_selection)
		move_up(new_selection)
	
	if Input.is_action_just_pressed("ui_accept"):
		player.pickup_item(self.shop_items[selection])
		$AnimationPlayer.play("FadeOut")
		exit_shop(player)
		
func on_animation_finished(anim_name: String):
	if anim_name == "FadeOut":
		queue_free()
		print("DEBUG: Queue free")
