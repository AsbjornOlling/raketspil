extends Node

class_name Item

var icon: StreamTexture
var index: int
var level: int

func _init(level: int) -> void:
	print("WARNING: Initializing generic item with level %d" % level)


func affect(player: KinematicBody2D) -> void:
	print("WARNING: Generic item invoking affect.")


func unaffect(player: KinematicBody2D) -> void:
	print("WARNING: Generic item invoking unaffect.")
