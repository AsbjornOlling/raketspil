extends Node2D

onready var links = $Links
var direction: Vector2 = Vector2(0, 0)
var tip: Vector2 = Vector2(0, 0)

export (float, 1, 100) var speed = 50

# kig på alt det her self self self

var throwing = false
var hooked = false

func throw(dir: Vector2) -> void:
	direction = dir.normalized()
	throwing = true
	tip = self.global_position

func release() -> void:
	throwing = false
	hooked = false

func _process(_delta):
	var player = get_parent()
	self.visible = throwing or hooked
	if not self.visible:
		return
	# For easier math later
	var tip_pos = to_local(tip)
	links.rotation = player.rotation
	$Tip.rotation = player.rotation
	links.position = tip_pos
	links.region_rect.size.y = tip_pos.length()
	
func _physics_process(delta):
	$Tip.global_position = tip
	if throwing:
		if $Tip.move_and_collide(direction * speed):
			hooked = true
			throwing = false
	# reset tip
	tip = $Tip.global_position
