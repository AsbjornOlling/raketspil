extends Item

class_name FuelContainer

# fuel stuff
var LEVEL_FUEL_MAP = {
		1: 50,
		2: 100,
		3: 150,
		4: 200
}
var _fuel: int

func _init(level: int).(level) -> void:
	# set inventory icon
	self.icon = load("res://assets/hud/inventory/item_icons/fuel_icon.png")
	
	# set level
	self.level = level
	
	# set stats according to level
	if not LEVEL_FUEL_MAP.has(level):
		print("ERROR! Fuel container does not have level %d" % level)
	else:
		self._fuel = LEVEL_FUEL_MAP[level]


func affect(player: KinematicBody2D) -> void:
	player.max_fuel += self._fuel


func unaffect(player: KinematicBody2D) -> void:
	player.max_fuel -= self._fuel
	if player.fuel >= player.max_fuel:
		player.fuel = player.max_fuel
