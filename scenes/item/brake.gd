extends Item

class_name Brake

func _init(level: int).(level) -> void:
	# set inventory icon
	self.icon = load("res://assets/hud/inventory/item_icons/brake_icon.png")
	
	# set level
	self.level = level
	# TODO: actually implement this item

func affect(player: KinematicBody2D) -> void:
	print("[WARNING]: Brake item not yet implemented")

func unaffect(player: KinematicBody2D) -> void:
	print("[WARNING]: Brake item not yet implemented")
