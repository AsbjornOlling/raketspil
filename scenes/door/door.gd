extends Area2D

export(String, FILE, "*.tscn") var world_scene
# Change what level the door leads to in the 
# 2d editor -> Door -> inspector -> script variables


func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	# returns an array of overlapping bodies. Player is a body
	for body in bodies:
		if body.name == "Player":
	  # TODO: only change the `Level` node
			get_tree().change_scene(world_scene)


func _on_DetectArea_body_entered(body):
	if body.name == "Player":
		$AnimatedSprite.play('openDoor')


func _on_DetectArea_body_exited(body):
	if body.name == "Player":
		$AnimatedSprite.play('closeDoor')
