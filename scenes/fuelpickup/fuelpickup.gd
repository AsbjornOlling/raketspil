extends Area2D

export (int, 1, 100) var fuelreward = 10;

func _ready():
	connect("body_entered", self, "_entered")


func _entered(body):
	# if Player hits the fuel pickup 
	# give them fuel and delete self
	if body.name == "Player":
		body.add_fuel(fuelreward)
		queue_free()
