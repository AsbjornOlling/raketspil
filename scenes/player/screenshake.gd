extends Node

onready var _camera = get_parent()
onready var _player = get_parent().get_parent()
var amplitude = 0

var THROTTLE_AMPLITUDE_MAP = {
#	-2: 2,
#	-1: 2,
#	0: 0,
#	1: 0,
#	2: 4,
#	3: 12,
#	4: 16
	-2: 0,
	-1: 0,
	0: 0,
	1: 0,
	2: 0,
	3: 3,
	4: 5
}

func _ready():
	$Duration.connect("timeout", self, "_on_duration_timeout")
	$Frequency.connect("timeout", self, "_on_frequency_timeout")
	self.start()


func _process(_delta):
	# set screenshake when throttling
	# XXX: seems like this is not a good way to do it
	# TODO: tweak this
	var amp = THROTTLE_AMPLITUDE_MAP[_player.throttle]
	start(-1, 16, amp)


func start(duration = 0.4, frequency = 16, amplitude = 4):
	self.amplitude = amplitude
	
	if duration > 0:
		$Duration.wait_time = duration
		$Duration.start()

	$Frequency.wait_time = 1 / float(frequency)
	$Frequency.start()
	_shake(self.amplitude)


func _shake(amp):
	var rand = Vector2()
	rand.x = rand_range(-amp, amp)
	rand.y = rand_range(-amp, amp)
	
	$Tween.interpolate_property(_camera, "offset", _camera.offset,
			rand, $Frequency.wait_time,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$Tween.start()


func _on_frequency_timeout():
	_shake(amplitude)


func _on_duration_timeout():
	_shake(0) # stop shaking
	$Frequency.stop()
