extends CPUParticles2D


var THROTTLE_SCALE_MAP = {
	-2: 0,
	-1: 0,
	0: 0,
	1: 0.4,
	2: 0.6,
	3: 1,
	4: 1.2,
	5: 1.7
}


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var throttle = get_parent().throttle
	set_emitting(throttle != 0)
	set_scale(Vector2(THROTTLE_SCALE_MAP[throttle], 1))
