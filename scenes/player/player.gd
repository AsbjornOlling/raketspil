extends KinematicBody2D

export(float, 0, 5) var TURN_SPEED = PI/2
export(float, 0, 1) var bounceCoefficient = 0.4
export(float, 1, 2) var boostSpeed = 1
# fuel
export(float, 10, 300) var max_fuel = 300
var fuel = max_fuel
# health
export(float, 10, 300) var max_health = 160
var health = max_health

var velocity = Vector2()
onready var screenshake = get_node("Camera2D/ScreenShake")

#shop
var in_shop = false

# USED BY HIT ITEMS
func add_fuel(amount):
	# this function is used by fuel pads
	fuel += amount 

func lose_fuel(amount):
	# this function is used by damage objects
	fuel -= amount
	
	
# HEALTH
var regen_timer = 0
export (int, 0, 1000) var time_before_regen = 100
export (float, 0, 2) var regen_amount = 0.5  # HP/s
export (float, 0, 2) var min_collision_dmg = 0.2
export (float, 0, 159) var max_damage = 100

func add_health(amount):
	health += amount
	
func gain_health_over_time(delta):
	regen_timer += delta
	if regen_timer >= time_before_regen:
		if health <= (max_health - (delta * regen_amount)):
			add_health(delta * regen_amount)
	
func stop_gaining_health():
	regen_timer = 0

func lose_health(amount):
	stop_gaining_health()
	health -= amount
	if health < 0:
		health = 0
		_death()
		
func collision_health_penalty(vel):
	# just trying out different values
	var difficulty = [0.4, 0.6, 1, 4]
	var health_penalty = min(max_damage, vel.length() * difficulty[3])
	# Don't take damage if you're scraping against the wall / design choice
	if health_penalty > min_collision_dmg:
	# Take damage according to your velocity
		lose_health(health_penalty)
	
	
func get_boosted(vector):
	velocity += vector
	

# DEATH
export(int, 0, 20) var deathTimerLimit = 10
var _deathTimer = 0

func _run_death_timer(delta):
	if _deathTimer < deathTimerLimit:
		_deathTimer += delta
	else:
		_death_timer_ended()


func _death_timer_ended():
	_death()


func _death():
	# this function might call many functions in the future.
	# for now, it just restarts the game.
	get_tree().change_scene("res://scenes/world.tscn")


# INVENTORY SYSTEM
#var FuelContainer = load("res://scenes/item/fuel_container.gd")

var inventory: Array = [null, null, null, null, null] 
						
var inventory_size: int = 3


func pickup_item(item: Item):
	for i in range(inventory_size):
		if inventory[i] == null:
			inventory[i] = item
			break


# ENGINE SYSTEM
# Engine has settings for each of the four throttle steps
# (and the two reverse)
var FORWARD_ENGINECONF = [
		{"fuel": 1, "accel": 5},  # low
		{"fuel": 4, "accel": 10}, # lowish 
		{"fuel": 10, "accel": 20}, # standard
		{"fuel": 15, "accel": 40}, # fast
]
var BACKWARD_ENGINECONF = [
		{"fuel": 12, "accel": -1}, # go backwards lol
		{"fuel": 13, "accel": -2}
]
var NEUTRAL_ENGINECONF = {"fuel": 0, "accel": 0}


func engine_accel_vector(delta, conf):
	# pure function that returns the acceleration vector,
	# given a time delta and an engine setting
	if fuel <= conf["fuel"]:
		# no fuel, do nothing
		return Vector2(0, 0)
	else:
		# use fuel and accelerate ship
		return Vector2(cos(rotation), sin(rotation)) * delta * conf["accel"]


func run_engine(delta) -> void:
	# effectful procedure that accelerates the player and uses fuel
	var engineconf = NEUTRAL_ENGINECONF
	if throttle > 0:
		engineconf = FORWARD_ENGINECONF[throttle - 1]
	elif throttle < 0:
		engineconf = BACKWARD_ENGINECONF[(-1 * throttle) - 1]

	# accelerate using engine
	velocity += engine_accel_vector(delta, engineconf)
	# use fuel
	fuel -= engineconf["fuel"] * delta

# THROTTLE SYSTEM
# -2 and -1 are backwards
# 0 is neutral
# 1, 2, 3, 4 and 5 are forwards
# a lot of other systems depend on the player's throttle setting
var MIN_THROTTLE = -2
var MAX_THROTTLE = 4
var throttle = 0


func set_throttle():
	# read input and set throttle
	if in_shop == false:
		if Input.is_action_just_pressed("ui_down") and MIN_THROTTLE < throttle:
			throttle -= 1
		if Input.is_action_just_pressed("ui_up") and throttle < MAX_THROTTLE:
			throttle += 1
		if Input.is_action_just_pressed("ui_x") or \
			Input.is_action_just_pressed("ui_c"):
			throttle = 0
		if Input.is_action_just_pressed("ui_1"):
			throttle = 1
		if Input.is_action_just_pressed("ui_2"):
			throttle = 2
		if Input.is_action_just_pressed("ui_3"):
			throttle = 3
		if Input.is_action_just_pressed("ui_4"):
			throttle = 4
	else:
		throttle = 0

# UPDATE FUNCIONS
func _process(delta):
	# player is disabled if no fuel, return early
	if fuel <= 0:
		_run_death_timer(delta)
		throttle = 0
		return
		
	gain_health_over_time(delta)
	
	# update engine and throttle systems
	run_engine(delta)
	set_throttle()


func _physics_process(delta):
	# actual physics stuff
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal) * bounceCoefficient
		screenshake.start()
		print(collision.collider)
		if collision.collider.has_method("hit"):
			collision.collider.hit(self)
		# lose health based on velocity
		collision_health_penalty(velocity)

	if fuel > 0:
		# handle rotation
		if in_shop == false:
			if Input.is_action_pressed("ui_right"):
				rotate(delta * TURN_SPEED)
			if Input.is_action_pressed("ui_left"):
				rotate(-1 * delta * TURN_SPEED)
