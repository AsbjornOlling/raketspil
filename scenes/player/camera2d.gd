extends Camera2D

var zoom_timer = 0
var standard_zoom = 1
var zoomed_in = 0.6 # a smaller number means that it's zoomed in further
var zoom_step = 0.004

func _process(delta):
	var player = get_parent()
	if player.in_shop == true:
		#if zoom_timer < 50:
		if zoom.x > zoomed_in + zoom_step:
			zoom.x -= zoom_step
			zoom.y -= zoom_step
	elif player.in_shop == false:
		if zoom.x < standard_zoom - zoom_step:
			zoom.x += zoom_step
			zoom.y += zoom_step
