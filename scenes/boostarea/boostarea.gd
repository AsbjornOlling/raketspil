extends Area2D

export (float, 0, 100) var BOOST_AMOUNT = 50
var RANGE = 16
onready var _player = get_node('/root/Root/Player')
var _affecting: Array


func _ready():
	self._affecting = []
	connect("body_entered", self, "_entered")
	connect("body_exited", self, "_exited")


func _process(delta):
	for body in self._affecting:
		# test if center is inside square
		# this prevents multiple boost pads from affecting the player
		if position.distance_to(body.position) < RANGE:
			var boostvec = delta * BOOST_AMOUNT * Vector2(cos(rotation), sin(rotation))
			body.get_boosted(boostvec)


func _entered(body):
	if body == _player:
		print("adding player")
		self._affecting.append(body)


func _exited(body):
	if not self._affecting.has(body):
		print("ERROR! Trying to remove non-existent body from affecting array.")
	self._affecting.erase(body)
