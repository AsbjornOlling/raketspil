extends KinematicBody2D

# floating up and down
var float_speed = 0
var float_accel = 6
var move_direction = "none"
var timer:int = 0
var timer_end = 0

# animation
var active = false
var selected = false

# for despawning
var spawn_point = Vector2()

func _ready():	
	# to avoid the bubbles floating next to each other:
	if name == "Bubble0":
		timer_end = 40
		active = true
	elif name == "Bubble1":
		timer_end = 80
	elif name == "Bubble2":
		timer_end == 120
		
	spawn_point = position
		

func _process(delta):
	# let the bubbles float from y: -8 to y: 0 and back again	
	if selected == false:
		if (timer > timer_end):
			if position.y >= -2:
				move_direction = "up"
			elif position.y <= -2:
				move_direction = "down"
		else:
			timer += 1
			move_direction = "none"
	
	else: # The player has selected this bubble
		$Sprite.speed_scale = 4
		move_direction = "up_fast"
	
			
func _physics_process(delta):
	move(move_direction, delta)
		
func move(dir, delta):
	if dir == "up":
		float_accel = -0.3
	elif dir == "down":
		float_accel = 0.3
	elif dir == "up_fast":
		float_accel = -3
	
	if move_direction != "none":
		float_speed += float_accel * delta
	position.y += float_speed
		
