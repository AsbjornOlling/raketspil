extends Control

onready var player = get_node("/root/Root/Player")

var viewport_size: = Vector2.ZERO
var overlay_size: = Vector2.ZERO
var pos: = Vector2.ZERO

# The Pause node is by default not visible.
# Every Node's class has the a 'Pause' attribute which can be toggled
# from the inspector.
# The three modes are:
# 1) Inherit (from parent)
# 2) Stop (stop this from running when the game is paused)
# 3) Process (keep this node running when paused).
# This node is currently the only node using the Process mode,
# as you need to be able to use the pause menu while paused.

func _input(event):
	if event.is_action_pressed('ui_p'):
		var new_pause_state = not get_tree().paused
		get_tree().paused = new_pause_state
		visible = new_pause_state
		draw_pause_menu()

func draw_pause_menu():
	viewport_size = OS.get_window_size()
	overlay_size = Vector2(viewport_size.x / 2, viewport_size.y / 2)
	# center the pause menu
	pos = Vector2(viewport_size.x / 2 - overlay_size.x / 2, viewport_size.y / 2 - overlay_size.y / 2)
	rect_size = overlay_size
	rect_position = pos
	
	# Buttons
	var buttons = $Buttons
	var i = 1
	var margin = 16
	for button in buttons.get_children():
		var button_y_offset = (margin + button.rect_size.y) * i
		button.rect_size.x = overlay_size.x / 4
		button.rect_position = Vector2(pos.x - button.rect_size.x / 2, button_y_offset)
		print(button.name)
		print(button.rect_position.y)
		i += 1
	


func _on_RestartButton_pressed():
	# To do: Make the restart button work.
	# This doesn't work: player._death()
	pass
